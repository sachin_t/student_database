import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:student_database/utility/routes_binding.dart';

import 'controller/student_controller.dart';


late final database;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final directory = await getApplicationDocumentsDirectory();
  final databasePath = '${directory.path}/student_table.db';
  database = await openDatabase(databasePath, version: 1,
      onCreate: (Database db, int version) async {
    await db.execute('''
    CREATE TABLE IF NOT EXISTS Students(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        age INTEGER,
        grade TEXT
      )
    ''');
  });
  Get.put(StudentController());
  bool isDeviceDark = getAppThemeState();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isDarkMode = prefs.getBool('isDarkMode') ?? false;
  if (isDeviceDark) {
    prefs.setBool('isDarkMode', true);
    isDarkMode = isDeviceDark;
  } else {
    prefs.setBool('isDarkMode', isDeviceDark);
    isDarkMode = isDeviceDark;
  }
  runApp(MyApp(isDarkMode));
}

class MyApp extends StatelessWidget {
  final bool isDarkMode;

  const MyApp(this.isDarkMode, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Student App',
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      themeMode: isDarkMode ? ThemeMode.dark : ThemeMode.light,
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    );
  }
}

bool getAppThemeState() {
  var brightness = SchedulerBinding.instance.window.platformBrightness;
  bool isDeviceDark = brightness == Brightness.dark; //to detect if device dark theme is enabled or not
  print('isDark == ${isDeviceDark.toString()}');
  return isDeviceDark;
}
