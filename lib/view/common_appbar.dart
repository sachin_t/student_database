import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/student_controller.dart';

PreferredSizeWidget commonAppbar(StudentController studentController, String? text) {
  return AppBar(
    leading: InkWell(
      onTap: () => Get.back(),
      child: Icon(Icons.keyboard_backspace_outlined,color: studentController.isDark.value
          ? Colors.white
          : Colors.black),
    ),
    backgroundColor: studentController.isDark.value
        ? Colors.black
        : Colors.white,
    title: Text(
      text??'',
      style: TextStyle(
          color: studentController.isDark.value
              ? Colors.white
              : Colors.black),
    ),
    actions: [GetX<StudentController>(
        builder: (context) {
          return toggleTheme(studentController);
      }
    )],
  );
}

Switch toggleTheme(StudentController studentController) {
  return Switch(
      value: studentController.isDark.value,
    thumbIcon: MaterialStateProperty.resolveWith ((Set  states) {
      if (states.contains(MaterialState.selected)) {
        return Icon(
          Icons.brightness_medium_outlined, color: Colors.black,);
      } else {
        return Icon(
         studentController.isDark.value?
         Icons.brightness_medium_outlined:
         Icons.nightlight_outlined, color: Colors.black,);
      }
    }),
    onChanged: (value) {
      studentController.changeTheme();
      studentController.update();
    });
}
