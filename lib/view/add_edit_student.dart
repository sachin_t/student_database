import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:student_database/view/widget/custom_textfield.dart';

import '../controller/student_controller.dart';
import '../models/student_model.dart';
import 'common_appbar.dart';

class AddEditStudentPage extends GetView<StudentController> {

  final StudentController studentController = Get.find(); //Dependency injection

  AddEditStudentPage({super.key});

  @override
  Widget build(BuildContext context) {
    final Student? selectedStudent = Get.arguments;
    if (selectedStudent != null) {
      studentController.nameController.text = selectedStudent.name;
      studentController.ageController.text = selectedStudent.age.toString();
      studentController.gradeController.text = selectedStudent.grade;
    }

    return Scaffold(
      appBar: commonAppbar(studentController, selectedStudent != null ?
      'Edit Student' : 'Add Student'),
      body: Padding(
        padding: const EdgeInsets.all(14.0),
        child: Form(
          key: studentController.formKey,
          child: Column(
            children: [
              CustomTextField(
                textEditingController: studentController.nameController,
                labelText: 'Name',
                controller: studentController,
                onChanged: (value) =>studentController.formKey.currentState!.validate(),
              ),
              const SizedBox(height: 10.0),
              CustomTextField(
                textEditingController: studentController.ageController,
                  labelText: 'Age',
                textInputType: TextInputType.number,
                onChanged: (value) =>studentController.formKey.currentState!.validate(),
                controller: studentController,
              ),
              const SizedBox(height: 10.0),
              CustomTextField(
                textEditingController: studentController.gradeController,
                labelText: 'Grade',
                onChanged: (value) =>studentController.formKey.currentState!.validate(),
                controller: studentController,
              ),
              const SizedBox(height: 20.0),
              GetX<StudentController>(
                builder: (studentController) {
                  return ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: studentController.isDark.value ? Colors.grey : Colors.black,
                    ),
                    onPressed: () {
                      if (studentController.formKey.currentState!.validate()) {
                      if (selectedStudent != null) {
                        studentController.updateStudent(
                          selectedStudent,
                          studentController.nameController.text,
                          int.parse(studentController.ageController.text),
                          studentController.gradeController.text,
                        );
                      } else {
                        studentController.insertStudent(
                          studentController.nameController.text,
                          int.parse(studentController.ageController.text),
                          studentController.gradeController.text,
                        );
                      }} else {
                        studentController.formKey.currentState!.validate();
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 60.0, vertical: 10),
                      child: Text(
                        selectedStudent != null ?
                        'Update' : 'Add',
                      ),
                    ),
                  );
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
