import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/student_controller.dart';
import '../utility/routes_binding.dart';
import 'common_appbar.dart';
import 'widget/student_tile.dart';

class StudentHome extends GetView<StudentController> {
  const StudentHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<StudentController>(
      builder: (studentController) {
        return Scaffold(
              appBar: commonAppbar(studentController, 'Students list'),
              body: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
                child: ListView(
                  children: [
                    Table(
                      children: [
                        TableRow(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: studentController.isDark.value?
                              Colors.white:Colors.black
                            ),borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                            children: [
                              Column(children:[Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Name', style: TextStyle(fontSize: 20.0)),
                              )]),
                              Column(children:[Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Age', style: TextStyle(fontSize: 20.0)),
                              )]),
                              Column(children:[Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Grade', style: TextStyle(fontSize: 20.0)),
                              )]),
                            ]
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    studentController.students.isEmpty?
                        Container(
                          height: Get.height * .8,
                          child: Center(child: Text('No students available',
                          style: TextStyle(fontSize: 20))),
                        ):
                    Container(
                      height: Get.height,
                      child: ListView.builder(
                        itemCount: studentController.students.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          var student = studentController.students[index];
                          return StudentCard(
                              student: student,
                              studentHomeController: studentController);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              floatingActionButton: FloatingActionButton(
                backgroundColor: studentController.isDark.value
                    ? Colors.white
                    : Colors.black,
                child: Icon(
                  Icons.add,
                  color: studentController.isDark.value
                      ? Colors.black
                      : Colors.white,
                ),
                onPressed: () => Get.toNamed(Routes.ADD_EDIT),
              ),
            );
      }
    );
  }
}
