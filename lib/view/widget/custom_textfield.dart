import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/student_controller.dart';

class CustomTextField extends StatelessWidget {
  TextEditingController textEditingController;
  String? labelText;
  StudentController controller;
  TextInputType? textInputType;
  Function(String value)? onChanged;
  CustomTextField({Key? key, required this.onChanged, required this.controller, required this.textEditingController,
    this.labelText, this.textInputType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetX<StudentController>(
      builder: (context) {
        return TextFormField(
          controller: textEditingController,
          onChanged: onChanged,
          validator: (value) {
            if (value!.isEmpty) {
              return '';
            }
            return null;
          },
          keyboardType: textInputType?? TextInputType.text,
          decoration: InputDecoration(
            labelText: labelText,
            enabledBorder: border,
            focusedBorder: border,
            errorBorder: errorBorder
          ),
        );
      }
    );
  }

  OutlineInputBorder get border => OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(6)),
    borderSide: BorderSide(color: controller.isDark.value?
    Colors.white:Colors.black)
  );

  OutlineInputBorder get errorBorder => const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(6)),
    borderSide: BorderSide(color: Colors.redAccent)
  );

}
