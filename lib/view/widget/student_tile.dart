import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/student_controller.dart';
import '../../models/student_model.dart';
import '../../utility/routes_binding.dart';

class StudentCard extends StatelessWidget {
  const StudentCard(
      {super.key, required this.student, required this.studentHomeController});
  final Student student;
  final StudentController studentHomeController;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      margin: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.grey.withOpacity(.2),
        border: Border.all(
          color: studentHomeController.isDark.value?
              Colors.white:Colors.black
        )
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                studentBox(student.name,context),
                studentBox(student.age.toString(),context),
                studentBox(student.grade,context),
              ],
            ),
          ),
          Divider(color: Colors.grey,thickness: 1),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                icon: const Icon(Icons.edit),
                onPressed: () =>
                    Get.toNamed(Routes.ADD_EDIT, arguments: student),
              ),
              Spacer(),
              IconButton(
                  icon: const Icon(Icons.delete),
                  onPressed: () =>
                      deleteDialog(student, studentHomeController)),
            ],
          )
        ],
      ),
    );
  }

  Future<dynamic> deleteDialog(
      Student student, StudentController studentHomeController) {
    return Get.defaultDialog(
      content: const Text(
        "Are you sure to delete this student's details?",
      ),
      confirm: ElevatedButton(
        style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
        onPressed: () {
          studentHomeController.deleteStudent(student);
          Get.back();
        },
        child: const Text(
          "Yes",
          style: TextStyle(color: Colors.white),
        ),
      ),
      cancel: ElevatedButton(
        style: ElevatedButton.styleFrom(
            elevation: 10, backgroundColor: Colors.white),
        onPressed: () {
          Get.back();
        },
        child: const Text(
          "No",
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }

  Widget studentBox(
      String data, BuildContext context) {
    return SizedBox(
      child: Row(
        children: [
          SizedBox(
            child: Text(data,
                maxLines: 1,
                style: TextStyle(
                  fontSize: 18,
                )),
          ),
        ],
      ),
    );
  }
}
