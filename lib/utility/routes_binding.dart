import 'package:get/get_navigation/src/routes/get_route.dart';

import '../controller/student_controller.dart';
import '../view/add_edit_student.dart';
import '../view/student_view.dart';

abstract class Routes {
  Routes._();

  static const HOME = _Paths.STUDENT_HOME;
  static const ADD_EDIT = _Paths.ADD_EDIT_PAGE;
}

abstract class _Paths {
  static const STUDENT_HOME = '/home';
  static const ADD_EDIT_PAGE = '/add_edit';
}

class AppPages {

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.STUDENT_HOME,
      page: () => const StudentHome(),
      binding: StudentBinding(),
    ),
    GetPage(
      name: _Paths.ADD_EDIT_PAGE,
      page: () => AddEditStudentPage(),
      binding: StudentBinding(),
    ),
  ];
}
