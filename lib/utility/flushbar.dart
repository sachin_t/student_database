import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';


  void showFlushBar({required String title, required BuildContext? context}) {
     Flushbar(
        backgroundColor: Colors.black87,
        icon: const Icon(Icons.warning_amber_outlined,color: Colors.white),
        borderRadius: BorderRadius.all(Radius.circular(8)),
        margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
        duration: const Duration(seconds: 3),
        messageText: Text(title,
            style: TextStyle(
            color: Colors.white, fontSize: 14,
            fontWeight: FontWeight.w600))).show(context!);

}
