import 'package:get/get.dart';

import '../main.dart';
import '../models/student_model.dart';
import '../utility/flushbar.dart';

class DatabaseHandler {

  var students = <Student>[].obs;

  Future<List<Student>> fetchStudents() async {
    try {
        final List<Map<String, dynamic>> maps = await database.query(
            'Students');
        students.value = List.generate(
          maps.length, (index) =>
            Student(
              id: maps[index]['id'],
              name: maps[index]['name'],
              age: maps[index]['age'],
              grade: maps[index]['grade'],
            ),
        );

        return students.value;

    } catch (e) {
      rethrow;
    }
  }

  Future<void> insertStudent(String name, int age, String grade) async {
    try {
      final id = await database.insert('Students', {
        'name': name,
        'age': age,
        'grade': grade,
      });

      print('id==$id');
      print('name==$name');
      print('name==$age');
      print('name==$grade');
      students.add(Student(id: id, name: name, age: age, grade: grade));
    } catch (e) {
      showFlushBar(title: e.toString(), context: Get.context);
    }
  }

  Future<void> updateStudent(
      Student student, String name, int age, String grade) async {
    try {
      student.name = name;
      student.age = age;
      student.grade = grade;
      await database!.update(
        'Students',
        {
          'name': name,
          'age': age,
          'grade': grade,
        },
        where: 'id = ?',
        whereArgs: [student.id],
      );
    } catch (e) {
      showFlushBar(title: e.toString(), context: Get.context);
    }
  }

  Future<void> deleteStudent(Student student) async {
    try {
      students.remove(student);
      await database.delete(
        'Students',
        where: 'id = ?',
        whereArgs: [student.id],
      );
    } catch (e) {
      showFlushBar(title: e.toString(), context: Get.context);
    }
  }

}
