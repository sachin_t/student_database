import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '../db_helper/database_handler.dart';
import '../models/student_model.dart';
import '../utility/flushbar.dart';

class StudentBinding implements Bindings {  //binding
  @override
  void dependencies() {
    Get.lazyPut<StudentController>(() => StudentController()); //Dependency injection
  }
}

class StudentController extends GetxController {

  final DatabaseHandler databaseHandler = DatabaseHandler();
  var students = <Student>[].obs;
  final TextEditingController nameController = TextEditingController(text: '');
  final TextEditingController ageController = TextEditingController(text: '');
  final TextEditingController gradeController = TextEditingController(text: '');
  final formKey = GlobalKey<FormState>();

  @override
  void onInit() async {
    super.onInit();

    fetchStudents();
    initTheme();

  }

  initTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDarkMode = prefs.getBool('isDarkMode') ?? false;
    Get.changeThemeMode(isDarkMode ? ThemeMode.dark : ThemeMode.light);
    isDark.value = Get.isDarkMode;
  }

  RxBool isDark = Get.isDarkMode.obs;

  void changeTheme() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    bool isDarkMode;
    try {
      isDarkMode = !prefs.getBool('isDarkMode')!;

      prefs.setBool('isDarkMode', isDarkMode);
      Get.changeThemeMode(isDarkMode ? ThemeMode.dark : ThemeMode.light);
      isDark.value = isDarkMode;
    } catch (e) {
      isDarkMode = true;
      prefs.setBool('isDarkMode', isDarkMode);
      Get.changeThemeMode(ThemeMode.dark);
      isDark.value = isDarkMode;
    }
  }

  void deleteStudent(Student student) async {
    await databaseHandler.deleteStudent(student).whenComplete(() => showFlushBar(title: "${student.name}'s details deleted successfully",
        context: Get.context));
    fetchStudents();
    update();
  }

  void fetchStudents() async {
    students.value = await databaseHandler.fetchStudents();
    update();
  }

  void updateStudent(Student selectedStudent, String name, int age, String grade) async {
    await databaseHandler.updateStudent(selectedStudent, name, age, grade).whenComplete(() {
      Get.back();
      fetchStudents();
      showFlushBar(title: "$name's details updated successfully",
          context: Get.context);
    });
    update();
  }

  void insertStudent(String name, int age, String grade) async {
    await databaseHandler.insertStudent(name, age, grade).whenComplete(() {
      Get.back();
      fetchStudents();
      showFlushBar(title: "$name's details added successfully",
          context: Get.context);
    });
    update();
  }

  Future<dynamic> deleteDialog(
      Student student) {
    return Get.defaultDialog(
      content: const Text(
        "Are you sure to delete this student's details?",
      ),
      confirm: ElevatedButton(
        style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
        onPressed: () {
          deleteStudent(student);
          Get.back();
        },
        child: const Text(
          "Yes",
          style: TextStyle(color: Colors.white),
        ),
      ),
      cancel: ElevatedButton(
        style: ElevatedButton.styleFrom(
            elevation: 10, backgroundColor: Colors.white),
        onPressed: () {
          Get.back();
        },
        child: const Text(
          "No",
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }

}
